package me.babayan;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MyClass {
    static int count = 0;

    @Override
    public boolean equals(Object obj) {
        return ++count > 1;
    }

    @Override
    public int hashCode() {
        return 118;
    }

    public static void main(String[] args) {
        final int MAX_INS = 5;


        // create MyClass instance and put to Set<MyClass>
        Set<MyClass> tSet = new HashSet<MyClass>();
        for (int i = 1; i <= MAX_INS; i++) {
            tSet.add(new MyClass());
        }

        //check and print the size of set
        System.out.println("size = " + tSet.size());



    }
}
